## Homework 03

Implement compose functions based on pictures. Pictures can be found in `screens/` folder.

## Homework submission
1. Create a private repository
- Go to the source repository (using Gitlab web interface).
- Click Fork.
- Click on your name, this creates your repository.
- Change the project visibility to private in *Settings -> General -> Permissions -> Project visibility -> Private.*
- Making project public (intentionally or unintentionally) means showing everybody your solution and can lead to point penalization! Double check the project visibility!

2. Add rights to your seminar tutor
- Go to *Members*
- Type his name and choose your seminar tutor
- Choose the role *Developer*
- Click *Add to project*
- **Adding other people except tutors is forbidden and will be penalized!**

3. Clone the repo to your computer, create new branch submit, do the homework and push it.

4. Go to the GitLab and create a merge request (MR).
- Go to *Merge Requests -> New merge request*
    - Source branch: **submit**
    - Target branch: **master** or **main** (merge against your own repository)
- Click *Compare branches* and continue.
    - Title: (leave the default)
    - Description: Leave empty or add some information you want to tell the teacher
    - Assignee: Your tutor
    - Make sure you have branches submit and master.
    - Click *Submit merge request*

Now you should see button Merge, but don't click it. Your tutor either accepts your solution or adds some notes on what you should change.

Make sure you really submitted what you wanted by clicking the tab Changes containing all changes of this MR.
