package com.example.newsfeed

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.newsfeed.ui.NewsFeedTopBar
import com.example.newsfeed.ui.screens.HomeScreen
import com.example.newsfeed.ui.theme.NewsFeedTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NewsFeedTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
                ) {
                    App()
                }
            }
        }
    }
}

@Composable
fun App() {
    // Here we could decide how to generate UI based on screen size
    NewsFeedAppPortrait()
}

@Composable
fun NewsFeedAppPortrait() {
    NewsFeedTheme {
        Scaffold(topBar = {
            // TODO("Add app bar here")
        }) { padding ->
            HomeScreen(modifier = Modifier.padding(padding))
        }
    }
}

@Preview(widthDp = 400, heightDp = 640)
@Composable
fun NewsFeedAppPortraitPreview() {
    NewsFeedAppPortrait()
}



