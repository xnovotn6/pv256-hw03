package com.example.newsfeed.models

import androidx.annotation.DrawableRes

data class ArticlePreview(
    val title: String,
    val perex: String,
    val author: Author,
    @DrawableRes val splashImage: Int,
    val readLengthMinutes: Int
)