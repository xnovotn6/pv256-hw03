package com.example.newsfeed.models

// In real world, profileImage would be String which holds url
data class Author(val name: String, val profileImage: Int)