package com.example.newsfeed.models

enum class NewsCategory {
    Newest,
    Local,
    World,
    Politics,
    Business,
    Sport,
    Arts,
    Opinion;
}