package com.example.newsfeed.repository

import com.example.newsfeed.R
import com.example.newsfeed.models.ArticlePreview
import com.example.newsfeed.models.Author

object FakeRepository {

    fun fetchNewsPreviews(): List<ArticlePreview> {
        return listOf(
            ArticlePreview(
                "Parents Protest Near Kindergarten",
                "\"We want to return to kindergarten - sleeping at lunch is better then working.\" says parents.",
                Author(name = "Mike Hunt", profileImage = R.drawable.nc_profile_picture_1),
                R.drawable.nc_background_image_1,
                7
            ),

            ArticlePreview(
                "The Future of Mobile Development",
                "Please, just abandon Java. It's ancient, chatty and contains many architectural failures.",
                Author(name = "Mike Oxlong", profileImage = R.drawable.nc_profile_picture_2),
                R.drawable.nc_background_image_2,
                12
            ),

            ArticlePreview(
                "High Temperatures Rewrites Historic Records",
                "February was way hotter. Unless you're me and you're smoking hot.",
                Author(name = "Ben Dover", profileImage = R.drawable.nc_profile_picture_3),
                R.drawable.nc_background_image_3,
                5
            ),

            ArticlePreview(
                "Customer at Targeted Demanded Flat Earth Map",
                "\"How dare you?! Let me speak to your manager!\", says Karen.",
                Author(name = "Anna Borshun", profileImage = R.drawable.nc_profile_picture_4),
                R.drawable.nc_background_image_4,
                3
            ),

            ArticlePreview(
                "Planes Are Awesome",
                "\"I even use plane when I go from my bedroom to bathroom. It's very fast and convenient.\", admits Taylor Swift",
                Author(name = "Dixie Normus", profileImage = R.drawable.nc_profile_picture_5),
                R.drawable.nc_background_image_5,
                22
            ),

            ArticlePreview(
                "Capybara",
                "Capybara, capybara, capybara, capybara. Capybaraaa. Capybaraaaaa",
                Author(name = "Jenna Tools", profileImage = R.drawable.nc_profile_picture_6),
                R.drawable.nc_background_image_6,
                1
            ),

            ).shuffled()
    }

}