package com.example.newsfeed.ui

import androidx.compose.material3.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

// TODO: Implement composable function which draws rounded box with article details (see README.md).
//  Function will accept Author and Int representing length of article in minutes.
//  You can add additional parameters if you need to.
//  ---
//  UI parameters:
//  Image - padding 4 dp
//  Texts - padding 8 dp, style labelSmall
//  Dot - color DarkGrey; height 4dp, width 4.dp
@Composable
fun ArticleDetails(/* TODO: Implement */) {
    Card() {
        TODO("Implement")
    }
}

// TODO: Implement preview of ArticleDetails
@Preview("Article Detail")
@Composable
private fun ArticleDetailsPreview() {
    TODO("Implement")
}