package com.example.newsfeed.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

// TODO: Implement compose function which shows image in circle shape (see README.md)
//  Function will accept drawable resource. contentDescription can be null.
//  You can add additional parameters if you need to.
//  ---
//  UI parameters:
//  Image size 32 dp
@Composable
fun CircleAvatar(/* TODO: Implement */) {
    TODO("Implement")
}

// TODO: Implement preview of CircleAvatar
@Preview("Circle Avatar")
@Composable
private fun NewsFeedCircleAvatar() {
    TODO("Implement")
}