package com.example.newsfeed.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.newsfeed.R
import com.example.newsfeed.models.ArticlePreview
import com.example.newsfeed.models.Author
import com.example.newsfeed.repository.FakeRepository
import com.example.newsfeed.ui.theme.NewsFeedTheme
import com.example.newsfeed.ui.theme.Typography

// TODO: Implement compose function which shows list of NewsCard
//  Function will accept list of ArticlePreview.
//  You can add additional parameters if you need to.
//  ---
//  UI parameters:
//  Space between two cards in list: 8 dp
//  Spacing from edge of screen: 8 dp
@Composable
fun NewsList(/* TODO: Implement */) {
    TODO("Implement")
}

// TODO: Implement compose function which shows single card with article
//  Function will accept ArticlePreview.
//  You can add additional parameters if you need to.
//  ---
//  UI parameters:
//  Height of image: 200 dp
//  Rounded corners of image: 12 dp
//  Spacing from edge of screen: 8 dp
//  Spacing of article details from edge of card: 8 dp
//  Spaces between texts/elements: 4 dp vertical, 16 horizontal
@Composable
fun NewsCard(/* TODO: Implement */) {
    OutlinedCard(
        border = BorderStroke(1.dp, Color.Black),
        modifier = modifier.fillMaxWidth(),
    ) {
        Box(
            /* TODO: Implement */
            contentAlignment = Alignment.BottomStart
        ) {
            /* TODO: Implement - image and article detail */
        }

        /* TODO: Implement */
    }
}

// TODO: Implement preview of NewsList
@Preview("News List",showBackground = true)
@Composable
fun NewsListPreview() {
    TODO("Implement")
}

// TODO: Implement preview of NewsCard
//  You can made up necessary data or use FakeRepository
@Preview("News Card", showBackground = true)
@Composable
fun NewsCardPreview() {
    TODO("Implement")
}