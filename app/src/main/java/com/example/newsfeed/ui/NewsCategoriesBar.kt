package com.example.newsfeed.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.newsfeed.models.NewsCategory

// TODO: Add functionality to compose function - category handling
//  News categories a horizontal list of chips of all categories. Selected category has tick on
//  left side. When different category is selected, tick goes to different category.
@Composable
fun NewsCategories(/* TODO: Implement */) {
    LazyRow(
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        // Careful, these are different PaddingValues
        contentPadding = PaddingValues(horizontal = 8.dp),
        modifier = modifier,
    ) {
        items(NewsCategory.entries) { item ->
            NewsCategoryChip(/* TODO: Implement */)
        }
    }
}

// TODO: Add functionality to compose function - category handling
//  NewsCategoryChip is chip which represent category. Selected category has tick on left side.
//  When different category is selected, tick goes to different category.
//  TASK: Update NewsCategories so it correctly updates tick to currently selected category.
//  TASK: Animate the change of tick.
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NewsCategoryChip(/* TODO: Implement */) {
    FilterChip(/* TODO: Implement */)
}

// TODO: Implement preview of NewsCategoryChip
@Preview("Category Chip - disabled", showBackground = true)
@Composable
fun NewsCategoryChipDisabledPreview() {
    TODO("Implement")
}

// TODO: Implement preview of NewsCategoryChip
@Preview("Category Chip - enabled", showBackground = true)
@Composable
fun NewsCategoryChipEnabledPreview() {
    TODO("Implement")
}

// TODO: Implement preview of NewsCategories
@Preview("News Categories", showBackground = true)
@Composable
fun NewsCategoriesPreview() {
    TODO("Implement")
}

