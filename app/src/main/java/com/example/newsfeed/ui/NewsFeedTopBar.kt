package com.example.newsfeed.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.newsfeed.R
import com.example.newsfeed.ui.theme.NewsFeedTheme

// TODO: This composable contains many of techniques you'll
//  reuse in your implementation. Try to understand logic but also structure/architecture of code.
@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun NewsFeedTopBar(
    @StringRes titleRes: Int,
    @DrawableRes profilePictureRes: Int,
    modifier: Modifier = Modifier,
    onActionClick: () -> Unit = {},
) {
    CenterAlignedTopAppBar(
        modifier = modifier,
        title = { Text(text = stringResource(id = titleRes)) },
        actions = {
            IconButton(onClick = onActionClick) {
                // TODO: Definitely not solution foreshadowing :clueless:
                CircleAvatar(drawable = profilePictureRes)
            }
        })
}

@Preview("Top App Bar")
@Composable
private fun NewsFeedTopBarPreview() {
    NewsFeedTheme {
        NewsFeedTopBar(
            titleRes = R.string.app_name,
            profilePictureRes = R.drawable.ab_profile_picture
        )
    }
}