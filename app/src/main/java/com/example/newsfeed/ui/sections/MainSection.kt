package com.example.newsfeed.ui.sections

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

// TODO: Implement compose function which acts as building block of screen - MainSection.
//  Main section consists of title and composable which it holds.
//  Parameters should be obvious from previous sentence.
//  ---
//  UI parameters:
//  title - text style titleLarge, font weight bold, padding horizontal 16 dp, vertical 8 dp
@Composable
fun MainSection(/* TODO: Implement */) {
    TODO("Implement")
}

// TODO: Implement preview of MainSection
@Preview(name = "Home Section", widthDp = 320, heightDp = 640, showBackground = true)
@Composable
fun MainSectionPreview() {
    TODO("Implement")
}