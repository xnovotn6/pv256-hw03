package com.example.newsfeed.ui.sections

import androidx.compose.runtime.Composable


// TODO: Implement compose function which acts as building block of screen - SecondarySection.
//  Secondary section consists of title and composable which it holds.
//  Parameters should be obvious from previous sentence.
//  ---
//  UI parameters:
//  title - text style titleMedium, font weight bold, padding horizontal 16 dp, vertical 8 dp
@Composable
fun SecondarySection(/* TODO: Implement */) {
    TODO("Implement")
}