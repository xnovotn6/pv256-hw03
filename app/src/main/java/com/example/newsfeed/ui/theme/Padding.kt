package com.example.newsfeed.ui.theme

// TODO: Define *data* class PaddingSizes for padding sizes. This useful to keep spacing consistency, especially
//  if you use well-defined design system (such as Material Design).
//  class will contain (at least) these three values:
//  - large
//  - medium
//  - small
//  All of these are of type density pixels (=> not Int type!!!)
//  You can also define more values if you need more granularity (e.g. extraSmall, extraLarge,...).
//  Check out naming in Typography - good source of inspiration.
class PaddingSizes()


// TODO: Define value Paddings which will hold instance of PaddingSizes.
//  Sizes will be:
//  - large: 16 density pixels
//  - medium: 8 density pixels
//  - small: 4 density pixels
// val Paddings = TODO("Implement padding")
